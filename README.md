# Práctica Final: JQuery, Ajax, DOM, JSON

## Descripción
Se modificó el archivo js/buscar.js. Me ayudé de AJAX y tomé la API de las peliculas que está en el archivo js/index.js para poder hacer la búsqueda de películas.
Al buscar la película, además del nombre, podemos ver la imagen y también la sinopsis y el estreno.



## Autora
* Gutiérrez González Martha Rubí