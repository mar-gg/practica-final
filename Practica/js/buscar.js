$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");

	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
		alert('Vamos a buscar: '+palabra);
		/*PONGA AQUI SU CÓDIGO*/

		$.ajax({
			url: "https://api.themoviedb.org/3/movie/upcoming?api_key=3356865d41894a2fa9bfa84b2b5f59bb&language=es"+palabra,
			success: function(respuesta) {
				console.log(respuesta);
				miLista.empty();
				$.each(respuesta.results, function(index, elemento) {
					miLista.append('<li>'+
						'<img src="https://image.tmdb.org/t/p/w500/'+elemento.poster_path+'" alt="'+elemento.original_title+'">'+
						'<h2>'+elemento.original_title+'</h2>'+
						'<p>Estreno: '+elemento.release_date+'</p>'+
						'<p>Sinopsis: '+elemento.overview+'</p>'+
					'</li>');
				});
			},
			error: function() {
				console.log("No se ha podido obtener la información");
			},
			beforeSend: function() {
				console.log('CARGANDO');
				miLista.empty();
				miLista.append('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
			},
		});
		
		

	});

});


